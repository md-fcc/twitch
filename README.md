My website using the Twitch API to fulfill the [FreeCodeCamp challenge](https://www.freecodecamp.org/challenges/use-the-twitchtv-json-api).

Live website: [Who's Twitching?](https://md-fcc.gitlab.io/twitch/)

The Twitch API used was provided by FreeCodeCamp, for example: https://wind-bow.glitch.me/twitch-api/channels/{name}
where {name} is a Twitch streamer's name.

The code is relatively straightforward and uses global variables and functions for simplicity. The userNames array global variable defines which users are displayed on the website. The main code is a chain of promises that does the following:
- calls the Twitch API for each user name
- stores API responses in an array of User objects
- adds HTML elements for each user

The chain of promises does not use any "catch" statements for errors. If an error occurs, 
the user shows up as offline with the message "Unable to find channel for login {name}."

I learned a lot about Javascript promises and found a few good articles to help me out in dealing with forEach loops and other nuances:
https://pouchdb.com/2015/05/18/we-have-a-problem-with-promises.html
https://www.datchley.name/es6-promises/
