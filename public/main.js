
// Define a list of Twitch users to display
let userNames = ['DreamHackCS', 'FreeCodeCamp', 'funfunfunction', 'Squirrel', 'szyzyg']


class User {
    // stores data about the user's channel and stream.
    // userData is unused because the information can be found in channel and stream.
    
    constructor(name) {
        this.name = name;
        //this.userData = null; /* store JSON from https://wind-bow.glitch.me/twitch-api/users/{name}     */
        this.channel = null;  /* store JSON from https://wind-bow.glitch.me/twitch-api/channels/{name}  */
        this.stream = null;   /* store JSON from https://wind-bow.glitch.me/twitch-api/streams/{name}   */
    }
}


function fetchUserJSON(name) {
    // calls each API to get information about the user and the stream.
    // userData is unused because the information can be found in channel and stream.
    
    return new Promise( (resolve, reject) => {

        //let getUserData = $.getJSON("https://wind-bow.glitch.me/twitch-api/users/" + name);
        let getChannelData = $.getJSON("https://wind-bow.glitch.me/twitch-api/channels/" + name);
        let getStreamData = $.getJSON("https://wind-bow.glitch.me/twitch-api/streams/" + name);
    
        $.when(getChannelData, getStreamData)
            .done((channelResponse, streamResponse) => {
                // responses are in the format of:
                // [content, "sucess" or "error", xhrObject]

                let user = new User(name);
                //user.userData = userResponse[0];
                user.channel = channelResponse[0];
                user.stream = streamResponse[0].stream;

                //console.log(user);
                resolve(user);
            });
    });

}


function buildUserList(userNames) {
    // creates an array of User objects
    let promises = userNames.map(fetchUserJSON);
    return Promise.all(promises);
}


function updateHTML(userList) {
    // Creates HTML elements for each user.
    // uses a promise so that errors are not swollowed in the chain of promises.
    
    return new Promise(function (resolve, reject) {
        
        //console.log(userList);
        
        let onlineCount = 0;

        userList.forEach( (user, index, array) => {
            
            
            let userUrl = "https://www.twitch.tv/" + user.name;
            
            // default to offline
            let statusOnline = false;
            let statusText = user.channel.error ? user.channel.message : "Offline";
            let descr = "";
            let cssClass = "offline";
            
            // check if online
            if (user.stream) {
                statusOnline = true;
                statusText = "Online";
                cssClass = "online";
                descr = "<p>" + user.stream.game + "</p>";
                onlineCount += 1;
            }
            
            let htmlContent = 
                "<div class=\"content " + cssClass + "\"" + ">" +
                    "<a href=" + userUrl + " target=\"_blank\">" +
                        "<img src=" + user.channel.logo + " width=\"100px\" height=\"100px\">" +
                    "</a>" +
                    "<div class=\"text\">" +
                        "<h3>" +
                            "<a class=" + cssClass + " href=" + userUrl + " target=\"_blank\">" + user.name + "</a>" +
                        "</h3>" +
                        "<p>" + statusText + "</p>" +
                        descr +
                    "</div>" +
                "</div>"

            if (statusOnline) {
                $("#userList-online").append(htmlContent);
            } else {
                $("#userList-offline").append(htmlContent);
            }

            // resolve when the forEach loop is done
            if (index === (array.length - 1)) {               
                if (onlineCount == 0) {
                    $("#userList-online").append("<p>No one is online</p>");
                } else if (onlineCount == userList.length) {
                    $("#userList-offline").append("<p>Everyone is online</p>");
                }
                resolve("success");
            }
        });
        
    });
}

// -----------------------------------------------------------------------------
// ------------------------------ MAIN -----------------------------------------
// -----------------------------------------------------------------------------

buildUserList(userNames)
    .then(updateHTML);




